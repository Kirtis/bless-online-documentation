How to contribute to the docs
==============================

.. toctree::
   :maxdepth: 1
   :name: toc-community-how_to_contribute_to_the_docs

   getting_started
   formatting_guide_lines
   naming_conventions